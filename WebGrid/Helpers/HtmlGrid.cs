﻿

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using WebGrid.DataTables;

namespace WebGrid.Helpers
{
    public static class HtmlGrid
    {
        public static MvcHtmlString DesenharGrid<TModel>(IEnumerable<TModel> model, DataTable dataTable)
        {
            StringBuilder CorpoGrid = new StringBuilder();
            CorpoGrid.Append("<table class=\"table table-condensed table-hover footable \"> ");                       
            CorpoGrid.Append("    <thead>");           
            CorpoGrid.Append("        <tr>");
            foreach (var item in dataTable.Colunas)
            {
                CorpoGrid.Append("        <th class=\"" + item.cssClass + "\" style=\"" + item.style + "\">");
                CorpoGrid.Append(item.LabelColuna);
                CorpoGrid.Append("        </th>");    
            }
            CorpoGrid.Append("        </tr>");    
            CorpoGrid.Append("    </thead>");
            CorpoGrid.Append("    <tbody>");

            int colindex = 0; string indice = ""; string nome = "";
            int rowindex = 1;
            if (model is IEnumerable)
            {
                foreach (var item in (IEnumerable)model)
                {
                    Type type = item.GetType();
                    PropertyInfo[] properties = type.GetProperties();

                    CorpoGrid.Append("    <tr id=\"" + rowindex + "\">");
                    
                    foreach (PropertyInfo p in properties)
                    {
                        //propriedade indice é apenas para a primeira celula
                        if (colindex == 0)
                        {
                            CorpoGrid.Append("        <td id=\"" + p.GetValue(item, null) + "\" indice=\"" + p.GetValue(item, null) + "\">");
                            indice = p.GetValue(item, null).ToString();
                        }
                        else if (colindex == 1) //coluna descrição
                        {
                            CorpoGrid.Append("        <td id=\"" + p.GetValue(item, null) + "\" >");
                            nome = p.GetValue(item, null).ToString();
                        }
                        else
                            CorpoGrid.Append("        <td id=\"" + p.GetValue(item, null) + "\" >"); 
                        
                        CorpoGrid.Append(p.GetValue(item, null)); 
                        CorpoGrid.Append("   <input type=\"hidden\" name=\"Especies[" + (rowindex - 1) + "]." + p.Name + "\" value=\"" + p.GetValue(item, null) + "\" />");                      
                        CorpoGrid.Append("        </td>");
                        colindex++;
                    }
                    CorpoGrid.Append("        <td>");
                    foreach (var button in dataTable.Acoes)
                    {
                        CorpoGrid.Append("        <a class=\"" + button.Classe +  "\" title=\"" + button.Titulo + "\" href=\"" + button.HRef + indice + "\" onclick=\"" + button.MetodoOnClick + "\" id=\"" + rowindex + "\" name=\"" + nome + "\">");
                        CorpoGrid.Append("            <i class=\"" + button.Icone + "\"></i>");
                        CorpoGrid.Append("        </a>");
                    }
                    CorpoGrid.Append("        </td>");
                    CorpoGrid.Append("   </tr>");
                    rowindex++;
                    colindex = 0;
                }
            }

            CorpoGrid.Append("    </tbody>");
            CorpoGrid.Append("</table>");

            return MvcHtmlString.Create(CorpoGrid.ToString());
        }
    }

}


