﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGrid.DataTables;
using WebGrid.Models;

namespace WebGrid.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ListaViewModel viewModel = new ListaViewModel();

            ColumnBuilder colunasespecies = new ColumnBuilder();
            AcaoBuilder acoesespecies = new AcaoBuilder();
            viewModel.DataTablePessoas.Colunas = colunasespecies.NovaColuna("", "Id").ComStyle("width: 10%;").ToColuna()
                                                                 .NovaColuna("", "Nome").ComStyle("width: 30%;").ToColuna()
                                                                 .NovaColuna("", "Telefone").ComStyle("width: 10%;").ToColuna()
                                                                 .NovaColuna("", "Ações").ComStyle("width: 10%;").ToColuna()
                                                                 .ToTable();
            viewModel.DataTablePessoas.Acoes = acoesespecies.NovaAcao("Visualizar", "Home/Visualizar/", "return VisualizarPessoa(this.id, this.name)", "glyphicon glyphicon-eye-open icon-white", "btn btn-info btn-small").ToAcao();
            viewModel.DataTablePessoas.Acoes = acoesespecies.NovaAcao("Editar", "#", "return EditarPessoa(this.id, this.name)", "glyphicon glyphicon-pencil icon-white", "btn btn-warning btn-small").ToAcao();
            viewModel.DataTablePessoas.Acoes = acoesespecies.NovaAcao("Remover", "Home/Create/", "return RemoverPessoa(this.id, this.name)", "glyphicon glyphicon-trash icon-white", "btn btn-danger btn-small").ToAcao();

            Pessoa p = new Pessoa();
            Pessoa p2 = new Pessoa();
            Pessoa p3 = new Pessoa();

            p.PessoaID = 1;
            p.Nome = "Alberto";
            p.Celular = "123456";

            p2.PessoaID = 2;
            p2.Nome = "Carlos";
            p2.Celular = "789456";

            p3.PessoaID = 3;
            p3.Nome = "Roberto";
            p3.Celular = "456978";

            viewModel.Pessoas = new List<Pessoa>();
            viewModel.Pessoas.Add(p);
            viewModel.Pessoas.Add(p2);
            viewModel.Pessoas.Add(p3);

            return View(viewModel);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Visualizar(int id)
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}