﻿using WebGrid.DataTables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebGrid.Models
{
    public class ListaViewModel
    {
        private DataTable _DataTablePessoas;
        public DataTable DataTablePessoas
        {
            get { return _DataTablePessoas ?? (_DataTablePessoas = new DataTable()); }
            set { _DataTablePessoas = value; }
        }

        public List<Pessoa> Pessoas { get; set; }
    }

    public class Pessoa
    {
        public int PessoaID { get; set; }
        public string Nome { get; set; }
        public string Telefone { get; set; }
        public string Celular { get; set; }
    }
}