﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebGrid.DataTables
{
    public class DataTable
    {
        public List<Coluna> Colunas { get; set; }

        public List<Acao> Acoes { get; set; }
    }

    public class Coluna
    {
        public string NomeColuna { get; set; }
        public string LabelColuna { get; set; }
        public string cssClass { get; set; }
        public string style { get; set; }
    }
    public class Acao
    {
        public string Titulo { get; set; }
        public string HRef { get; set; }
        public string MetodoOnClick { get; set; }
        public string Icone { get; set; }
        public string Classe { get; set; }
    }

    public class ColumnBuilder
    {

        private List<Coluna> colunas = new List<Coluna>();

        private Coluna coluna;

        public ColumnBuilder NovaColuna(string NomeColuna, string LabelColuna)
        {
            this.coluna = new Coluna();
            this.coluna.LabelColuna = LabelColuna;
            this.coluna.NomeColuna = NomeColuna;
            return this;
        }

        public ColumnBuilder ComCssClass(string cssClass)
        {
            this.coluna.cssClass = cssClass;
            return this;
        }

        public ColumnBuilder ComStyle(string style)
        {
            this.coluna.style = style;
            return this;
        }

        public ColumnBuilder ToColuna()
        {
            this.colunas.Add(this.coluna);
            return this;
        }

        public List<Coluna> ToTable()
        {
            return this.colunas;
        }
    }

    public class AcaoBuilder
    {

        private List<Acao> acoes = new List<Acao>();

        private Acao acao;

        public AcaoBuilder NovaAcao(string Titulo, string HRef, string MetodoOnClick, string Icone, string Classe)
        {
            this.acao = new Acao();
            this.acao.Titulo = Titulo;
            this.acao.HRef = HRef;
            this.acao.MetodoOnClick = MetodoOnClick;
            this.acao.Icone = Icone;
            this.acao.Classe = Classe;
            this.acoes.Add(acao);
            return this;
        }

        public List<Acao> ToAcao()
        {
            return this.acoes;
        }
    }
}